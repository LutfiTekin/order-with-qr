package dev.into.orderwithqr;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RestoranLandingPageActivity extends AppCompatActivity {

    TextView name,slogan,cat;
    ImageView logo;
    RecyclerView ordersRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restoran_landing_page);
        logo = findViewById(R.id.image);
        name = findViewById(R.id.name);
        slogan = findViewById(R.id.slogan);
        cat = findViewById(R.id.cat);
        ordersRV = findViewById(R.id.ordersRV);
        ordersRV.setLayoutManager(new LinearLayoutManager(this));
        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if(intent==null) return;
        Bundle bundle = intent.getExtras();
        name.setText(bundle.getString(OrderWithQR.NAME,"n/A"));
        slogan.setText(bundle.getString(OrderWithQR.SLOGAN,"n/A"));
        cat.setText(bundle.getString(OrderWithQR.CATEGORY,"n/A"));
        Glide.with(this).load(bundle.getString(OrderWithQR.LOGO,"default")).error(R.mipmap.ic_launcher).into(logo);
        String id = bundle.getString(OrderWithQR.ID);
        try {
            FirebaseDatabase.getInstance().getReference().child(OrderWithQR.RESTAURANTS).child(id).child("menus").addListenerForSingleValueEvent(valueEventListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            List<OrderMenu> list = new ArrayList<>();
            for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                list.add(snapshot.getValue(OrderMenu.class));
            }
            ordersRV.setAdapter(new orderAdapter(list));
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private class orderAdapter extends RecyclerView.Adapter<orderAdapter.ViewHolder> {

        private List<OrderMenu> orderList;

        public orderAdapter(List<OrderMenu> orderList) {
            this.orderList = orderList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int i) {
            holder.name.setText(orderList.get(i).name);
            holder.description.setText(orderList.get(i).description);
            holder.price.setText(orderList.get(i).price);
            Glide.with(RestoranLandingPageActivity.this).load(orderList.get(i).image).into(holder.image);
        }

        @Override
        public int getItemCount() {
            return orderList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            ImageView image;
            TextView name,description,price;

            ViewHolder(LayoutInflater inflater, ViewGroup parent) {
                super(inflater.inflate(R.layout.order_menu_object, parent, false));
                image = itemView.findViewById(R.id.image);
                name = itemView.findViewById(R.id.name);
                description = itemView.findViewById(R.id.description);
                price = itemView.findViewById(R.id.price);
            }

            @Override
            public void onClick(View v) {

            }
        }

    }

}
