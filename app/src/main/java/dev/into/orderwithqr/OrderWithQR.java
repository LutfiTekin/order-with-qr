package dev.into.orderwithqr;

/**
 * OrderWithQR Created by lutfi on 27.05.2017.
 */

public class OrderWithQR {
    public final static String ID = "id";
    public final static String NAME = "name";
    public final static String SLOGAN = "slogan";
    public final static String CATEGORY = "category";
    public final static String LOGO = "logo";
    public final static String RESTAURANTS = "restaurants";
}
