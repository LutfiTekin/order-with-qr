package dev.into.orderwithqr;

/**
 * OrderWithQR Created by lutfi on 27.05.2017.
 */

public class OrderMenu {
    public String name,image,price,description;

    public OrderMenu() {
    }

    public OrderMenu(String name, String image, String price, String description) {
        this.name = name;
        this.image = image;
        this.price = price;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
