package dev.into.orderwithqr;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;

public class BarcodeActivity extends AppCompatActivity {

    TextView textView;
    SurfaceView cameraview;
    FirebaseDatabase database;
    DatabaseReference reference;
    boolean IS_QR_MATCHED_ALREADY = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);
        cameraview = findViewById(R.id.cam_surface);
        textView = findViewById(R.id.textView);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        database = FirebaseDatabase.getInstance();
        reference = database.getReference().child(OrderWithQR.RESTAURANTS);
        reference.keepSynced(true);
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        final CameraSource cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setAutoFocusEnabled(true)
                .setRequestedFps(60)
                .setRequestedPreviewSize(480, 480)
                .build();
        cameraview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(BarcodeActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    cameraSource.start(holder);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    textView.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {
                            String code = barcodes.valueAt(0).displayValue;
                            textView.setText(code);
                            verifycode(code);
                        }
                    });
                }
            }
        });
    }

    private void verifycode(final String code) {
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    if(dataSnapshot.child(code).exists() && !IS_QR_MATCHED_ALREADY){
                        DataSnapshot selectedRestaurant = dataSnapshot.child(code);
                        Intent landingpage = new Intent(BarcodeActivity.this,RestoranLandingPageActivity.class);
                        landingpage.putExtra(OrderWithQR.NAME,selectedRestaurant.child(OrderWithQR.NAME).getValue(String.class));
                        landingpage.putExtra(OrderWithQR.SLOGAN,selectedRestaurant.child(OrderWithQR.SLOGAN).getValue(String.class));
                        landingpage.putExtra(OrderWithQR.LOGO,selectedRestaurant.child(OrderWithQR.LOGO).getValue(String.class));
                        landingpage.putExtra(OrderWithQR.CATEGORY,selectedRestaurant.child(OrderWithQR.CATEGORY).getValue(String.class));
                        landingpage.putExtra(OrderWithQR.ID,code);
                        startActivity(landingpage);
                        IS_QR_MATCHED_ALREADY = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        IS_QR_MATCHED_ALREADY = false;
        super.onStart();
    }
}
